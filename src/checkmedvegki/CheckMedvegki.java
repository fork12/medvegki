/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package checkmedvegki;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Element;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import javax.swing.*;


/**
 *
 * @author fork
 */
public class CheckMedvegki {
    private static Connection conn;
    public static void main(String[] args) {
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:medvegki.db");
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println("Error message:" + ex.getMessage());
            return;
        }

        if((args.length>0) && (args[0].equals("config"))) {
            frmWinConfig app = new frmWinConfig(conn);
            app.setLocationRelativeTo(null);
            app.setVisible(true);
            
        } else {
            int id;
            String s_mail="<html><head></head><body>";
            String s;
            boolean fl=false;
            for(int i=1,j=getPageCount();i<=j;i++){
                id=updatePage(i);
                if(id>=0) {
                    s=comparePage(i,id);
                    if(!s.isEmpty()) {
                        if(!s.equals("first")){
                            fl=true;
                            s_mail+=getPageTitle(i)+s;
                        }
                    } else deletePage(id);
                }
            }
            if(fl) {
                s_mail+="</body></html>";
                sendMail(s_mail);
            } else System.out.println("Изменений нет");

        }
    }
    private static int getPageCount(){
        return(5);
    }

    private static String getPageTitle(int page){
        String s="";
        switch(page){
            case 1: s="<h2><a href=\""+getPageURL(page)+"\">Публичные слушания:</a></h2>"; break;
            case 2: s="<h2><a href=\""+getPageURL(page)+"\">Нормативные документы Администрации:</a></h2>";  break;
            case 3: s="<h2><a href=\""+getPageURL(page)+"\">Проекты документов:</a></h2>";  break;
            case 4: s="<h2><a href=\""+getPageURL(page)+"\">Нормативные документы Совета Депутатов:</a></h2>";  break;
            case 5: s="<h2><a href=\""+getPageURL(page)+"\">Муниципальные Программы поселения:</a></h2>";  break;
        }
        return(s);
    }

    private static String getPageURL(int page){
        String s="";
        switch(page){
            case 1: s="http://www.spmedvezhye-ozerskoe.ru/dokumenty/publichnye-slushaniya/";  break;
            case 2: s="http://www.spmedvezhye-ozerskoe.ru/dokumenty/Oficialny-dokumenty/normativnye-dokumenty-Administracii/";  break;
            case 3: s="http://www.spmedvezhye-ozerskoe.ru/dokumenty/Oficialny-dokumenty/proekty-dokumentov/";  break;
            case 4: s="http://www.spmedvezhye-ozerskoe.ru/dokumenty/Oficialny-dokumenty/normativnye-dokumenty-Soveta-Deputatov/";  break;
            case 5: s="http://www.spmedvezhye-ozerskoe.ru/dokumenty/munuzcipalnye-programmy-poseleniya/"; break;
        }
        return(s);
    }
    
    private static String getGlobalURL(int page,String s){
        String gu="http://www.spmedvezhye-ozerskoe.ru";
        s=s.replaceFirst(" href=\"", " href=\""+gu);
        return(s);
        
    }
    
    private static Elements getPageElements(int page,Document doc){
        Elements el=null;
        switch(page){
            case 1: ;
            case 2: ;
            case 3: ;
            case 4: el=doc.select("div.small_news"); break;
            case 5: el=doc.select("div.prog").select("a"); break;
        }
        return(el);
    }
    
    
    private static String getTextElement(int page,Element el){
        String s;
        s="";
        switch(page){
            case 1: ;
            case 2: ;
            case 3: ;
            case 4: s=el.select("a").first().outerHtml(); break;
            case 5: s=el.outerHtml(); break;
        }
        return(s);
    }

    
    private static int updatePage(int page){
        int id_upd=-1;
        Document doc = null;
        try {
            doc = Jsoup.connect(getPageURL(page)).get();
        } catch (Exception ex) {
            System.out.println("Error message:" + ex.getMessage());
        }
        if (doc != null) {
            Elements news = getPageElements(page, doc);
            try {
                Statement statement = conn.createStatement();
                statement.setQueryTimeout(5);
                // создаем таблицы если их нет
                statement.executeUpdate("create table if not exists tbl_updates (id integer primary key AUTOINCREMENT, id_page int, dt integer default CURRENT_TIMESTAMP)");
                statement.executeUpdate("create table if not exists tbl_pages (id_update integer, context text)");

                //создаем новый блок обновление
                statement.executeUpdate("Insert into tbl_updates (id_page) values (" + page + ")");
                ResultSet rs = statement.executeQuery("SELECT last_insert_rowid() as id");
                rs.next();
                id_upd = rs.getInt("id");
                String s;

                //заносим текущее состояние в базу и сравниваем с предыдущим апдейтом
                for (Element el : news) {
                    s = getTextElement(page,el);
                    statement.executeUpdate("Insert into tbl_pages values (" + id_upd + ",'" + s + "')");
                }
            } catch (Exception ex) {
                System.out.println("Error message:" + ex.getMessage());
            }
        }
        return(id_upd);
        
    }

    private static void deletePage(int id_upd){
            try {
                Statement query = conn.createStatement();
                query.setQueryTimeout(5);
                query.executeUpdate("Delete from tbl_updates where id="+id_upd+";Delete from tbl_pages where id_update="+id_upd);
            } catch (Exception ex) {
                System.out.println("Error message:" + ex.getMessage());
            }
       
    }
    
    private static String comparePage(int page,int id_upd){
            //получаем ид предыдущего состояния
            int id_prev=-1;
            String dt_prev;
            String s_mail="";
            try {
                Statement statement = conn.createStatement();
                statement.setQueryTimeout(5);
                ResultSet rs = statement.executeQuery("SELECT id,dt from tbl_updates where id_page="+page+" and id<>"+id_upd+" order by dt desc limit 1");
                if(rs.next()){
                    id_prev=rs.getInt("id");
                    dt_prev=rs.getString("dt");

                    //сравниваем с предыдущим апдейтом
                    //сначала новые
                    rs = statement.executeQuery("SELECT p1.context from tbl_pages as p1 left join tbl_pages as p2 on p2.id_update="+id_prev+" and p1.context=p2.context where p1.id_update="+id_upd+" and p2.context is null");
                    s_mail="";
                    String pref = "<div><span>Новые записи:</span>";
                    boolean fl_ch=false;
                    while(rs.next()){
                        s_mail+=pref+"<div>"+getGlobalURL(page,rs.getString(1))+"</div>";
                        pref=""; fl_ch=true;
                    }
                    if(pref.isEmpty()) s_mail+="</div>";
                    //потом удаленные
                    if(id_prev>=0){
                        rs = statement.executeQuery("SELECT p1.context from tbl_pages as p1 left join tbl_pages as p2 on p2.id_update="+id_upd+" and p1.context=p2.context where p1.id_update="+id_prev+" and p2.context is null");
                        pref = "<div><span>Удаленные записи:</span>";
                        while(rs.next()){
                            s_mail+=pref+"<div>"+getGlobalURL(page,rs.getString(1))+"</div>";
                            pref="";  fl_ch=true;
                        }
                        if(pref.isEmpty()) s_mail+="</div>";
                    }
                } else s_mail="first";
            } catch (Exception ex) {
                System.out.println("Error message:" + ex.getMessage());
            }
            return(s_mail);
        
    }
    private static void sendMail(String body){
        try {
            Statement query = conn.createStatement();
            query.setQueryTimeout(5);
            //получаем конфиг
            ResultSet rs = query.executeQuery("SELECT * from tbl_config limit 1");
            if(rs.next()){
                String username = rs.getString("emailFrom");
                String password = rs.getString("emailPass");
                
                Properties props = new Properties();
                props.put("mail.smtp.auth", "true");
                props.put("mail.smtp.starttls.enable", "true");
                props.put("mail.smtp.host", rs.getString("smtpServer"));
                props.put("mail.smtp.port", rs.getString("smtpPort"));

                Session session = Session.getInstance(props, new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(username));
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(rs.getString("emailTo")));
                message.setSubject(rs.getString("emailSubj"));
                message.setContent(body, "text/html; charset=UTF-16");
                Transport.send(message);
            }
        } catch (SQLException | MessagingException ex) {
                System.out.println("Error message:" + ex.getMessage());
        }
        
    }
}

            

